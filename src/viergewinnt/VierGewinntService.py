import random
# TODO: Library einbinden, um Webservice zu starten
import web

# TODO: Web Application mit URL erstellen
urls = (
    '/viergewinnt', 'VierGewinnt'
)
app = web.application(urls, globals())
# Beispiel-Aufruf mit 1 besetztem Feld:
# http://127.0.0.1:8080/viergewinnt?fields=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20b


class VierGewinnt:
    # TODO: auf GET reagieren und GET-Parameter "fields" auslesen
    # TODO (EK): "fields" auf Gültigkeit pruefen
    # TODO: next_move mit "fields" aufrufen und Ergebnis als Text zurückliefern
    def GET(self):
        # Url Parameter einlesen
        # Siehe: https://webpy.readthedocs.io/en/latest/input.html
        i = web.input(fields="NA")
        # Auf Gültigkeit überprüfen
        if check_input(i.fields):
            # Bei gültiger Eingabe next_move aufrufen und Ergebnis zurückgeben
            return next_move(i.fields)
        else:
            return "Fehler - Falsche Paramater überliefert!"


def check_input(field: str) -> bool:
    """
    Überpüft Eingabe auf Gütligkeit
    :param field: Erhaltener URL-Parameter
    :return: True, wenn alle Anforderungen erfüllt, ansonsten False
    """
    # Testen ob Parameter gesetzt wurde
    if field is "NA":
        return False
    # Testen ob Laenge richtig ist
    if len(field) != 16:
        return False
    # Feld auf valide Zeichen ueberpruefen
    for c in field:
        if c != ' ' and c != 'b' and c != 'r':
            return False
    return True


def next_move(field: str):
    """
    Versucht, mit zwei bzw. einem Zug eine Gewinnbedingung zu erreichen
    :param field: Das aktuelle 4x4-Spielfeld
    :return: Das neue 4x4-Spielfeld (inkl. gesetztem Feld)
    """
    # Gewinnbedingung mit einem Zug
    i = 0
    # Alle vier Spalten durchgehen
    for i in range(4):
        j = -1
        # Bis zum letzten freien Feld dieser Spalte navigieren
        while (j+1)*4+i < 16 and field[(j+1)*4+i] == ' ':
            j+=1
        # Es ist noch etwas frei
        if j >= 0:
            # Stein in diesem Feld einsetzen und probieren, ob gewonnen wird
            index = (j)*4+i
            trywin = field[:index] + 'r' + field[index + 1:]
            if checkwin(trywin) == 'r':
                return trywin

    # Gewinnbedingung mit zwei Zuegen
    # Vertikal testen
    for i in range(4):
        if field[i+12] == 'r' and field[i+8] == 'r' and field[i+4] == ' ':
            print(field[:i+4] + 'r' + field[i+5:])
            return field[:i+4] + 'r' + field[i+5:]
    # Horizontal testen
    for i in range(4):
        row = field[12 - i * 4:16 - i * 4]
        if i != 0:
            row_under = field[12 - (i-1) * 4:16 - (i-1) * 4]
            if row_under.count(' ') > 0:
                break
        if row.count(' ') == 2 and row.count('r') == 2:
            position = row.index(' ')
            print(field[:12-(i*4)+position] + 'r' + field[13-(i*4)+position:])
            return field[:12-(i*4)+position] + 'r' + field[13-(i*4)+position:]

    # Es kann nicht mit 1 Zug gewonnen werden: Zufaellige freie Spalte waehlen
    if field[:15].count(' ') > 0:
        j = -1
        num = -1
        while j < 0:
            num = random.randint(0, 4)
            while (j+1)*4+num < 16 and field[(j + 1) * 4 + num] == ' ':
                j += 1
        index = (j)*4+num
        fields = field[:index] + 'r' + field[index + 1:]
    return fields


def checkwin(fields):
    """
    Prueft, ob im uebergebenen Spielfeld jemand gewonnen hat (r oder b).
    :param fields: das zu ueberpruefende Spielfeld
    :return: der Gewinner (r, b oder ' ')
    """
    win = ' '
    # Alle Gewinnbedingungen pruefen
    if fields[0] == fields[1] == fields[2] == fields[3]:
        win = fields[0]
    elif fields[4] == fields[5] == fields[6] == fields[7]:
        win = fields[4]
    elif fields[8] == fields[9] == fields[10] == fields[11]:
        win = fields[8]
    elif fields[12] == fields[13] == fields[14] == fields[15]:
        win = fields[12]
    elif fields[0] == fields[4] == fields[8] == fields[12]:
        win = fields[0]
    elif fields[1] == fields[5] == fields[9] == fields[13]:
        win = fields[1]
    elif fields[2] == fields[6] == fields[10] == fields[14]:
        win = fields[2]
    elif fields[3] == fields[7] == fields[11] == fields[15]:
        win = fields[3]
    elif fields[0] == fields[5] == fields[10] == fields[15]:
        win = fields[0]
    elif fields[3] == fields[6] == fields[9] == fields[12]:
        win = fields[3]
    return win


if __name__ == "__main__":
    # TODO: Webservice starten
    app.run()
