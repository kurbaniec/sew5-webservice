import sys

from PyQt5.QtWidgets import *
from PyQt5.QtCore import QCoreApplication
# TODO: View designen und generieren
# Mittels: pyuic5 -o VierGewinntView.py VierGewinnt.ui
from viergewinnt.view import VierGewinntView as MyView
# TODO: Model-File erstellen, Model-Klasse implementieren und hier importieren
from viergewinnt.model.mode import ModelError
from viergewinnt.model.online_mode import OnlineMode
from viergewinnt.model.offline_mode import OfflineMode

# TODO: Klasse und Methoden Kommentieren


class MyController(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.myForm = MyView.Ui_VierGewinnt()
        self.myForm.setupUi(self)
        self.fields = list(' '*16)
        self.display()
        # TODO: Model-Objekt erstellen und Button-Event Handler hinzufuegen
        self.mode = OnlineMode(self.fields)
        self.fallback_mode = OfflineMode(self.fields)
        # Applikation beenden wenn "Beenden"-Button gedrueckt wird
        self.myForm.exitButton.clicked.connect(lambda: QCoreApplication.quit())
        # Modus/Strategy aendern wenn zugehoerige Checkbox gedrueckt wurde
        self.myForm.checkboxService.clicked.connect(self.switch_mode)
        # Spielzug mit Model berechnen
        self.myForm.fieldButton.clicked.connect(self.process)
        # Neues Spiel starten
        self.myForm.newGameButton.clicked.connect(self.reset)

    def display(self, draw: bool = False, win: bool = False, winner: str = "NA"):
        """
        Fuehrt die Haupt-Ausgabe des Spiels am Textfeld durch.
        :param draw: Optionaler Parameter, der bei einem Unentschieden gesetzt werden soll
        :param win: Optionaler Parameter, der bei einem Gewinn gesetzt werden soll
        :param winner: Optional Parameter, der den Gewinnernamen setzt. Sollte gemeinsam mit
        dem Parameter win gesetzt werden
        """
        output = '------\n|'
        output += self.fields[0] + self.fields[1] + self.fields[2] + self.fields[3] + "|\n|"
        output += self.fields[4] + self.fields[5] + self.fields[6] + self.fields[7] + "|\n|"
        output += self.fields[8] + self.fields[9] + self.fields[10]+ self.fields[11] + "|\n|"
        output += self.fields[12] + self.fields[13] + self.fields[14]+ self.fields[15] + "|\n"
        output += '------'
        # TODO: Gewinn-Nachricht anzeigen, falls jemand gewonnen hat
        if draw:
            output += "\nUnentschieden!"
        elif win:
            output += "\nDer " + winner + " hat das Spiel gewonnen!"
        # TODO: in Textfeld anzeigen
        self.myForm.displayArea.setText(output)
        self.myForm.errorLabel.setText("")

    def display_error(self, text: str):
        """
        Wird benutzt um Fehler in der View dem Benutzer mitzuteilen.
        :param text: Fehlermeldung als String
        """
        self.myForm.errorLabel.setText(text)

    # TODO: bei Buttonklick Feld besetzen, Model aufrufen (Webservice abfragen) und Ergebnis anzeigen
    # TODO (EK): Error Handling einbauen
    def process(self):
        """
        Fuehrt die Logik eines Spielzugs mithilfe eines Models durch.
        Wird durch den Klick auf "Feld besetzen" ausgeloest.
        """
        try:
            # Benutzereingabe verarbeiten
            # Falls diese ungueltig ist, wird sie ignoriert
            if self.mode.process_input(self.myForm.fieldNumber.value()):
                # Spielzug mit der ausgewaehlten Strategie verarbeiten
                self.mode.process()
                # Schauen ob jemand gewonnen hat
                win, winner = self.mode.check_win()
                if win:
                    self.display(win=True, winner=winner)
                    self.myForm.fieldButton.setEnabled(False)
                else:
                    # Schauen ob Spiel unentschieden ausgegangen ist
                    if self.mode.check_draw():
                        self.display(draw=True)
                        self.myForm.fieldButton.setEnabled(False)
                    # Ansonsten weiter spielen
                    else:
                        self.display()
        except ModelError as e:
            # Fehler auf der View ausgeben
            self.mode.withdraw_input(self.myForm.fieldNumber.value())
            self.display_error(str(e))

    # TODO (EK): bei Checkbox-Klick Strategie aendern
    def switch_mode(self):
        """
        Tauscht die Strategie des Models. Wechselt zwischen Online- und Offline-Modus.
        """
        tmp = self.mode
        self.mode = self.fallback_mode
        self.fallback_mode = tmp

    def reset(self):
        """
        Setzt das Spiel zurueck.
        """
        self.fields.clear()
        self.fields.extend(list(' '*16))
        self.myForm.fieldButton.setEnabled(True)
        self.display()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = MyController()
    c.show()
    sys.exit(app.exec_())
