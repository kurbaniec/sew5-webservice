# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VierGewinnt.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_VierGewinnt(object):
    def setupUi(self, VierGewinnt):
        VierGewinnt.setObjectName("VierGewinnt")
        VierGewinnt.resize(1006, 824)
        self.centralwidget = QtWidgets.QWidget(VierGewinnt)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(-1, 20, -1, -1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.fieldNumber = QtWidgets.QSpinBox(self.centralwidget)
        self.fieldNumber.setMaximum(3)
        self.fieldNumber.setObjectName("fieldNumber")
        self.horizontalLayout_3.addWidget(self.fieldNumber)
        self.fieldButton = QtWidgets.QPushButton(self.centralwidget)
        self.fieldButton.setObjectName("fieldButton")
        self.horizontalLayout_3.addWidget(self.fieldButton)
        self.checkboxService = QtWidgets.QCheckBox(self.centralwidget)
        self.checkboxService.setChecked(True)
        self.checkboxService.setObjectName("checkboxService")
        self.horizontalLayout_3.addWidget(self.checkboxService)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.displayArea = QtWidgets.QTextEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(14)
        self.displayArea.setFont(font)
        self.displayArea.setObjectName("displayArea")
        self.verticalLayout.addWidget(self.displayArea)
        self.errorLabel = QtWidgets.QLabel(self.centralwidget)
        self.errorLabel.setText("")
        self.errorLabel.setObjectName("errorLabel")
        self.verticalLayout.addWidget(self.errorLabel)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.newGameButton = QtWidgets.QPushButton(self.centralwidget)
        self.newGameButton.setObjectName("newGameButton")
        self.horizontalLayout_2.addWidget(self.newGameButton)
        self.exitButton = QtWidgets.QPushButton(self.centralwidget)
        self.exitButton.setObjectName("exitButton")
        self.horizontalLayout_2.addWidget(self.exitButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        VierGewinnt.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(VierGewinnt)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1006, 31))
        self.menubar.setObjectName("menubar")
        VierGewinnt.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(VierGewinnt)
        self.statusbar.setObjectName("statusbar")
        VierGewinnt.setStatusBar(self.statusbar)

        self.retranslateUi(VierGewinnt)
        QtCore.QMetaObject.connectSlotsByName(VierGewinnt)

    def retranslateUi(self, VierGewinnt):
        _translate = QtCore.QCoreApplication.translate
        VierGewinnt.setWindowTitle(_translate("VierGewinnt", "VierGewinnt"))
        self.label.setText(_translate("VierGewinnt", "Feld (0-3):"))
        self.fieldButton.setText(_translate("VierGewinnt", "Feld besetzen"))
        self.checkboxService.setText(_translate("VierGewinnt", "Webservice"))
        self.newGameButton.setText(_translate("VierGewinnt", "Neues Spiel"))
        self.exitButton.setText(_translate("VierGewinnt", "Beenden"))

