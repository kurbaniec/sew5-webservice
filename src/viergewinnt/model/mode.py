from abc import ABC, abstractmethod
from typing import List


class Mode(ABC):
    """
    Abstrakte Klassen, die einen Vier gewinnt Modus darstellt.
    """

    def __init__(self, fields: List[str]):
        self.fields = fields

    @abstractmethod
    def process(self):
        """
        Wird benutzt um den Zug des Computer-Gegners zu berechnen.
        """
        pass

    def process_input(self, column: int) -> bool:
        """
        Validiert und fuerht den Spielerzug durch.
        :param column: Index der Spalte wo ein Element gesetzt werden soll
        :return: True, falls in der Spalte noch Platz ist, ansonsten False
        """
        if column < 0 or column > 3:
            raise ModelError("Ungültige Spaltenanzahl")
        empty_field = False
        for i in range(column + 12, column-1, -4):
            if self.fields[i] == ' ':
                self.fields[i] = 'b'
                empty_field = True
                break
        return empty_field

    def withdraw_input(self, column: int):
        """
        Nimmt den letzten Spielerzug zurueck, letzter Zug wird annuliert.
        :param column: Spalte wo ein Element gesetzt wurde
        """
        for i in range(column, column+13, 4):
            if self.fields[i] == 'b':
                self.fields[i] = ' '
                break

    def check_draw(self) -> bool:
        """
        Testet, ob das Spiel Unentschieden ausgegangen ist.
        :return: True, falls das Spiel Unentschieden ausgegangen ist, ansonsten False
        """
        draw = True
        for c in self.fields:
            if c == ' ':
                draw = False
                break
        return draw

    def check_win(self) -> (bool, str):
        """
        Testet, ob der Spieler oder Computer das Spiel gewonnen hat
        :return: True und Gewinnername, falls jemand das Spiel gewonnen hat, ansonsten False mit
        leerem String
        """
        # Horizontal testen
        for i in range(0, 16, 4):
            if self.fields[i] == self.fields[i+1] == self.fields[i+2] == self.fields[i+3]:
                if self.fields[i] == 'r' or self.fields[i] == 'b':
                    return True, determine_winner(self.fields[i])
        # Vertikal testen
        for i in range(0, 4):
            if self.fields[i] == self.fields[i+4] == self.fields[i+8] == self.fields[i+12]:
                if self.fields[i] == 'r' or self.fields[i] == 'b':
                    return True, determine_winner(self.fields[i])
        # Quer testen
        if self.fields[0] == self.fields[5] == self.fields[10] == self.fields[15]:
            if self.fields[0] == 'r' or self.fields[0] == 'b':
                return True, determine_winner(self.fields[0])
        if self.fields[3] == self.fields[6] == self.fields[9] == self.fields[12]:
            if self.fields[3] == 'r' or self.fields[3] == 'b':
                return True, determine_winner(self.fields[3])
        # Kein Gewinn, es wird weiter gespielt
        return False, ''


def determine_winner(field: str) -> str:
    """
    Wandelt Element 'b' oder 'r' in den Spielernamen druch.
    :param field: Element-Name
    :return: Der Spielername des Elements
    """
    if field == 'b':
        return "Spieler"
    return "Computer"


class ModelError(Exception):
    """
    Eigene Exception fuer etwaige Model-Fehler.
    """
    pass
