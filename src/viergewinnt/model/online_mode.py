from viergewinnt.model.mode import Mode, ModelError
import requests
from typing import List


class OnlineMode(Mode):
    """
    Repraesentiert den Online-Modus der das Webservice für Vier gewinnt verwendet.
    """

    def __init__(self, fields: List[str]):
        super().__init__(fields)

    def process(self):
        """
        Benutzt den Webservice um den naechsten Zug vom Computer zu berechnen.
        """
        # Url mit field Parameter für Webservice bilden
        url = "http://localhost:8080/viergewinnt?fields=" + ''.join(self.fields)
        # Anfrage durchfuehren
        try:
            resp = requests.get(url)
        # Jegliche Fehler abfangen
        except Exception as e:
            raise ModelError("Kann keine Verbindung zum Service herstellen")
        if resp.status_code != 200:
            raise ModelError("Ein unerwarteter Fehler ist aufgetreten")
        if resp.text.startswith("Fehler"):
            raise ModelError(resp.text)
        # Falls Anfrage erfolgreich war, field-Liste mit neuem Feld befuellen
        self.fields.clear()
        self.fields.extend(list(resp.text))
