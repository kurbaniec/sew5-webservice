from viergewinnt.model.mode import Mode
from typing import List


class OfflineMode(Mode):
    """
    Repraesentiert den Offline-Modus mit simplen Algorithmus für Vier gewinnt.
    """

    def __init__(self, fields: List[str]):
        super().__init__(fields)

    def process(self):
        """
        Erstes leeres Feld wird vom Computer besetzt.
        """
        for i in range(15, -1, -1):
            if self.fields[i] == ' ':
                self.fields[i] = 'r'
                break

