# "*Vier Gewinnt*" - Python Webservice

## Aufgabenstellung

Sie möchten eine vereinfachte textbasierte Variante des Spiels „Vier gewinnt“ implementieren. Der Computergegner wird über ein Webservice abgebildet, welches auf Basis des empfangenen Spielfelds den Zug des Computergegners berechnet und retourniert. Das Webservice soll unter http://127.0.0.1:8080/viergewinnt erreichbar sein und einen GET-Parameter namens „fields“ übernehmen, der das aktuelle Spielfeld darstellt. Als Rückgabe liefert das Webservice das neue Spielfeld (inkl. dem Zug des Gegners).

Das Spielfeld ist eine 16-stellige Zeichenkette (4x4), die als simpler Text (kein JSON) versendet wird. Ein Zeichen des Spielfelds kann entweder 'b' (für ein Feld des blauen Spielers), 'r' (für den roten Computergegner) oder ' ' (für ein leeres Feld) sein. Die Zeichenkette stellt das 4x4-Spielfeld von links nach rechts und von oben nach unten (beginnend links oben) dar:

![](images/angabe_1.png)

entspricht dem Spielfeld

![](images/angabe_2.png)

**Webservice**

In VierGewinntService.py befindet sich bereits eine Methode next_move, die versucht, mit einem Zug eine Gewinnbedingung für den Computergegner 'r' zu erreichen. Ist dies nicht möglich, wird eine zufällige freie Spalte gewählt. Das resultierende neue Spielfeld wird von next_move zurückgeliefert.

Erweitere VierGewinntService.py, sodass dieser Algorithmus als web.py-Webservice bereitgestellt wird und als Python-Skript gestartet werden kann!

**Grundanforderungen Webservice**

Folgende Anforderungen sollen erfüllt werden:

- Das VierGewinntService ist nach dem Starten unter http://127.0.0.1:8080/viergewinnt erreichbar und übernimmt ein Spielfeld als 16-stellige Zeichenkette als GET-Parameter namens fields. Verwende hierfür web.py!
- Das VierGewinntService ermittelt anhand des bestehenden Algorithmus übern next_move den nächsten Zug des Computergegners und liefert das neue Spielfeld als 16-stellige Zeichenkette als Text zurück (kein JSON)
- Der Code ist kommentiert (Python Docstrings). Es wird **keine HTML-Dokumentation** benötigt.

**Beispiel:**

Request (%20 entspricht einem Leerzeichen): [http://127.0.0.1:8080/viergewinnt?fields=%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20b](http://127.0.0.1:8080/viergewinnt?fields=               b)

Rückgabe:      r b

In diesem Beispiel empfing das Webservice die Zeichenkette:

![](images/angabe_3.png)

Bzw.

![](images/angabe_4.png)

Nachdem keine Gewinnbedingung mit nur einem Zug erreicht werden kann, wurde ein zufälliges Feld mit 'r' besetzt. In diesem Fall wurde daher folgendes Spielfeld vom Webservice zurückgeliefert:

![](images/angabe_5.png)

Bzw.

![](images/angabe_6.png)

**Erweiterungen Webservice**

Erfülle außerdem:

- Das Webservice überprüft den übergebenen fields-Parameter auf seine Gültigkeit (16 Stellen, nur die Zeichen ' ', 'b' und 'r') und liefert ggf. eine entsprechende Fehlermeldung als Zeichenkette zurück.
- Kann keine Gewinnbedingung mit 1 Zug erreicht werden, prüft der Algorithmus, ob er mit zwei Zügen eine Gewinnbedingung erreichen kann und verfolgt gegebenenfalls diese Strategie.

**Client**

Der Client ist eine Qt-Anwendung und erlaubt die Eingabe des Zugs des Spielers. Außerdem wird das aktuelle Spielfeld in einem Textfeld angezeigt. Auf den Zug des Spielers ('b') folgt immer gleich der Zug des Computers ('r'). Das Template enthält bereits Code für die Darstellung des Spielfelds als textbasierte 4x4-Matrix, welche noch in einem Textfeld angezeigt werden muss.

![](images/angabe_7.png)

**Grundanforderungen Client**

Erfülle folgende Grundanforderungen:

- Es wird das MVC-Pattern verwendet, sodass View, Controller und Model getrennt in separaten Source-Files gespeichert werden und eigene Klassen sind.

- Der Client besitzt ein Ausgabefeld, welches den Status des aktuellen Spielfelds als Text darstellt.

- Der Client bietet in den Grundanforderungen vier Eingabemöglichkeiten:

- - Feld (Zahl): Ermöglicht die Eingabe, in welche Spalte als nächstes ein 'b' gesetzt werden soll.
  - Feld besetzen (Button): Besetzt das unterste freie Feld der gewählten Spalte mit 'b' (sofern die Spalte noch frei ist) und schickt im Anschluss das aktuelle Spielfeld gleich an das Webservice, um den Zug des 'r'-Gegners zu ermitteln. Das Ergebnis beider Züge wird in der GUI im dargestellten Format angezeigt.
  - Neues Spiel (Button): Startet ein neues Spiel, sodass das Spielfeld und die Anzeige zurückgesetzt (geleert) werden.
  - Beenden (Button): Schließt den Client.

- Hat 'b' oder 'r' das Spiel gewonnen, wird eine entsprechende Nachricht angezeigt.

- Gibt man einen falschen Wert ein (z.B. bereits volle Spalte), wird die Eingabe ignoriert.

- Im Model passiert der Zugriff auf das Webservice, d.h. die Model-Klasse ist für die Übermittlung der Daten ans Webservice und die Rückgabe zuständig.

- Der Code ist kommentiert (Python Docstrings), Ausnahme: generierte View

**Erweiterungen Client**

Erfülle außerdem:

- Die grafische Oberfläche verhält sich responsiv, d.h. sie kann vergrößert und verkleinert werden, sodass die grafischen Komponenten mitskalieren.

- Bei der Abfrage des Webservices werden Fehler abgefangen (z.B. Webservice nicht verfügbar) und angezeigt.

- Es wird ein zweiter Modus unterstützt, wobei das Strategy-Design-Pattern eingesetzt wird:

- - In einer abstrakten Basisklasse werden die Gemeinsamkeiten beider Strategien abstrahiert.
  - Der Online-Modus verwendet das Webservice wie beschrieben.
  - Der Offline-Modus verwendet einen simplen Algorithmus (z.B. erstes leeres Feld oder Zufallsfeld).
  - Mit einem Klick auf die Checkbox „Webservice“ wird die Strategie geändert, sodass ab diesem Zeitpunkt ohne weitere if-Abfragen die aktuell ausgewählte Strategie verwendet wird.

## Ausführen

* `pip install -r requirements.txt`
* `python src/viergewinnt/VierGewinntController.py`
* `python src/viergewinnt/VierGewinntService.py`

Achtung: Falls mit PyCharm/IntelliJ gearbeitet wird, muss der `src`-Ordner als `Sources Root` markiert werden.

## Allgemeine Tipps

### Qt

* Designer starten:
  * Windows - `designer`
  * Linux - `./qtcreator`

* Responsive Layout

  * Neues Layout zum Main Window hinzufügen

    ![](images/qt_1.png)

  * Auf Hintergrund/Background klicken und `senkrecht anordnen` oder `Vertical Layout` auswählen

    ![](images/qt_2.png)

* `.ui` File zu Python-View konvertieren:

  `pyuic5 -o <output_filename>.py <input_filename>.ui`

  z.b. `pyuic5 -o VierGewinntView.py VierGewinnt.ui`

### Web.py

* URL-Handling: https://webpy.org/docs/0.3/tutorial#urlhandling
* Accessing User Input: https://webpy.readthedocs.io/en/latest/input.html

### Request

* https://requests.readthedocs.io/en/master/